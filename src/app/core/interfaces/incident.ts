export interface Incident {
  id: number;
  title: string;
  priority: number;
}
