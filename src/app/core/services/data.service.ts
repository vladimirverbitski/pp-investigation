import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Incident} from '../interfaces/incident';
import {filter, map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient
  ) { }

  getIncidentsList(): Observable<Incident[]> {
    return this.http.get<Incident[]>('assets/data/incidents.json');
  }

  getIncidentById(id: number): Observable<Incident> {
    return this.http.get<Incident[]>('assets/data/incidents.json')
      .pipe(
        map(data => data.find(it => it.id === Number(id)))
      );
  }
}
