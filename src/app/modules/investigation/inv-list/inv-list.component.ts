import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Incident} from '../../../core/interfaces/incident';
import {DataService} from '../../../core/services/data.service';

@Component({
  selector: 'app-inv-list',
  templateUrl: './inv-list.component.html',
  styleUrls: ['./inv-list.component.scss']
})
export class InvListComponent implements OnInit {

  incidentsList$: Observable<Incident[]>;

  constructor(
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    this.incidentsList$ = this.dataService.getIncidentsList();
  }

}
