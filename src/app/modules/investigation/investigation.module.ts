import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InvCardComponent} from './inv-card/inv-card.component';
import {InvListComponent} from './inv-list/inv-list.component';
import {InvDetailComponent} from './inv-detail/inv-detail.component';
import {InvestigationRoutingModule} from './investigation-routing.module';
import {ShareModule} from '../../share/share.module';
import {RouterModule} from '@angular/router';
import {NgxGrapgComponent} from './ngx-grapg/ngx-grapg.component';
import {NgxGraphModule} from '@swimlane/ngx-graph';
import {FormsModule} from '@angular/forms';
import { IsSelectedPipe } from './ngx-grapg/is-selected.pipe';


@NgModule({
    declarations: [InvCardComponent, InvListComponent, InvDetailComponent, NgxGrapgComponent, IsSelectedPipe],
    imports: [
        CommonModule,
        ShareModule,
        InvestigationRoutingModule,
        RouterModule,
        NgxGraphModule,
        FormsModule
    ]
})
export class InvestigationModule {
}
