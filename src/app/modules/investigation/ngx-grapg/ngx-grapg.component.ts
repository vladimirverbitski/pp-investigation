import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-ngx-grapg',
  templateUrl: './ngx-grapg.component.html',
  styleUrls: ['./ngx-grapg.component.scss']
})
export class NgxGrapgComponent implements OnInit {

  center$: Subject<boolean> = new Subject();
  zoomToFit$: Subject<boolean> = new Subject();
  isZoomEnabled = true;

  nodeName: string;
  clusterName: string;
  selectedNode;
  selectedGroup;
  selectedLink;
  clusterNodes = [];
  linkSource;
  linkTarget;

  links = [
    {
      id: 'link1',
      source: 'node1',
      target: 'node2',
      label: 'is parent of'
    }, {
      id: 'link2',
      source: 'node1',
      target: 'node3',
      label: 'custom label'
    }, {
      id: 'link3',
      source: 'node1',
      target: 'node4',
      label: 'custom label'
    }
  ];

  nodes = [
    {
      id: 'node1',
      label: 'node 1'
    }, {
      id: 'node2',
      label: 'node 2'
    }, {
      id: 'node3',
      label: 'node 3'
    }, {
      id: 'node4',
      label: 'node 4'
    }, {
      id: 'node5',
      label: 'node 5'
    }, {
      id: 'node6',
      label: 'node 6'
    }, {
      id: 'node7',
      label: 'node 7'
    }, {
      id: 'node8',
      label: 'node 8'
    }, {
      id: 'node9',
      label: 'node 9'
    }, {
      id: 'node10',
      label: 'node 10'
    }
  ];

  clusters = [
    {
      id: 'cluster1',
      label: 'Cluster 1',
      childNodeIds: ['node2', 'node3', 'node4']
    }, {
      id: 'cluster2',
      label: 'Basket',
      childNodeIds: ['node5', 'node6', 'node7', 'node8', 'node9', 'node10']
    }
  ];

  layoutSettings = {
    orientation: 'TB'
  };

  constructor() {
  }

  ngOnInit(): void {
  }

  addNode(artifact: string): void {
    const id = `node${this.nodes.length + 1}`;
    this.nodes.push({
      id,
      label: artifact
    });
    this.nodes = [...this.nodes];
  }

  addCluster(cluster, nodes): void {
    const id = `cluster${this.clusters.length + 1}`;
    this.clusters.push({
      id,
      label: cluster,
      childNodeIds: nodes
    });

    this.clusterNodes = [];
    this.clusters = [...this.clusters];
  }

  addLink(source, target, label?: string): void {
    const id = `link${this.links.length + 1}`;
    this.links.push({
      id,
      label,
      source,
      target
    });
    this.links = [...this.links];
  }

  onClusterClick(cluster): void {
    if (cluster) {
      this.selectedGroup = cluster;
    }
  }

  deleteCluster(clusterId): void {
    this.clusters = this.clusters.filter(it => it.id !== clusterId);
  }

  deleteNode(nodeId): void {
    this.nodes = this.nodes.filter(it => it.id !== nodeId);
  }

  onNodeClick(node): void {
    if (node) {
      this.selectedNode = node;
    }
  }

  onLinkClick(link): void {
    if (link) {
      this.selectedLink = link;
    }
  }

  deleteLink(linkId): void {
    this.links = this.links.filter(it => it.id !== linkId);
  }

  centerGraph(): void {
    this.center$.next(true);
  }

  fitGraph(): void {
    this.zoomToFit$.next(true);
  }

  zoomToggle(): void {
    this.isZoomEnabled = !this.isZoomEnabled;
  }

}
