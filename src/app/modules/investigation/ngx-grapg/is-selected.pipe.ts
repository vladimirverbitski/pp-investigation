import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isSelected'
})
export class IsSelectedPipe implements PipeTransform {

  transform(item, selectedItem): string {
    if (selectedItem) {
      return item.id === selectedItem.id ? 'selected' : '';
    }
  }

}
