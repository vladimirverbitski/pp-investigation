import {Component, Input, OnInit} from '@angular/core';
import {Incident} from '../../../core/interfaces/incident';

@Component({
  selector: 'app-inv-card',
  templateUrl: './inv-card.component.html',
  styleUrls: ['./inv-card.component.scss']
})
export class InvCardComponent implements OnInit {

  @Input() incident: Incident;

  constructor() { }

  ngOnInit(): void {
  }

}
