import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {InvListComponent} from './inv-list/inv-list.component';
import {InvDetailComponent} from './inv-detail/inv-detail.component';

const routes: Routes = [
  { path: '', component: InvListComponent},
  { path: ':id', component: InvDetailComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class InvestigationRoutingModule { }
