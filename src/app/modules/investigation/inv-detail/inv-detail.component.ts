import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';
import {DataService} from '../../../core/services/data.service';
import {Incident} from '../../../core/interfaces/incident';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-inv-detail',
  templateUrl: './inv-detail.component.html',
  styleUrls: ['./inv-detail.component.scss']
})
export class InvDetailComponent implements OnInit {

  item$: Observable<Incident>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService
  ) {
  }

  ngOnInit(): void {

    this.item$ = this.activatedRoute.params
      .pipe(
        map(it => it.id),
        switchMap(it => this.dataService.getIncidentById(it))
      );
  }

}
