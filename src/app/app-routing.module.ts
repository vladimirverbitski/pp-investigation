import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {path: '', pathMatch: 'full', redirectTo: '/investigation'},
    {
        path: 'investigation',
        data: {preload: true},
        loadChildren: () => import('./modules/investigation/investigation.module').then(m => m.InvestigationModule)
    },
    {path: '**', redirectTo: '/investigation'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
